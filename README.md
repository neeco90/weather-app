# Weather Finder Application

_This app allows you to search for a city and a country and get the current weather!_
_Even you can add favourite cities and find them easily_

## Pre-requisitos 📋
_You will need to get installed Node.js and Git in your PC. You can install it from here:_
* [Node.js](https://nodejs.org/es/)
* [Git](https://git-scm.com/downloads)

## First step ⚙️
_First of all, clone the repo to your PC_
```
git clone https://gitlab.com/neeco90/weather-app.git
```

### Installing 🔧
_In order to install all the packages necesaries you will have to run this command line_
```
npm install
```

## Starting the project 🚀
_To start the project, just run following command line_
```
npm start
```

## Built with 🛠️
* HTML5
* CSS3
* JavaScript
* [React.js](https://es.reactjs.org/)
* [Gulp.js](https://gulpjs.com/)
* [SASS](https://sass-lang.com/)
* [Flexboxgrid](http://flexboxgrid.com/)
* [Axios](https://github.com/axios/axios)
