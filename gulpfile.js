const gulp = require('gulp');
const sass = require('gulp-sass');
const rename = require('gulp-rename');

gulp.task('sass', function() {
  return gulp
    .src(['./src/resources/sass/main.scss'])
    .pipe(sass({ outputStyle: 'compressed' }).on('error', sass.logError))
    .pipe(rename('main.min.css'))
    .pipe(gulp.dest('./public/css'));
});

gulp.task('sass-watch', function() {
  gulp.watch('./src/resources/sass/**/*.scss', gulp.series('sass'));
});
