import React, { useEffect } from 'react';
import { PropTypes } from 'prop-types';

import { MESSAGES } from '../../config/messages';
import { useStateValue } from '../../state/index';

const Input = props => {
  const error = {};
  const touch = {};

  Input.propTypes = {
    type: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    classNameCustom: PropTypes.string,
    name: PropTypes.string.isRequired,
    customValue: PropTypes.string,
    checked: PropTypes.bool,
    disabled: PropTypes.bool,
    required: PropTypes.bool,
    readonly: PropTypes.bool,
    maxlength: PropTypes.number,
    icon: PropTypes.string,
    onChangeCustom: PropTypes.func
  };

  Input.defaultProps = {
    placeholder: '',
    classNameCustom: '',
    customValue: '',
    checked: false,
    disabled: false,
    required: false,
    readonly: false,
    maxlength: 256,
    icon: null,
    onChangeCustom: null
  };

  const { REQUIRED_FIELD } = MESSAGES;

  const [
    {
      app: { errors, touched }
    },
    dispatch
  ] = useStateValue();

  const validateNotEmpty = e => {
    if (!e.target.value) {
      error[e.target.name] = REQUIRED_FIELD;

      errors.push(error);

      dispatch({
        type: 'setErrors',
        newErrors: errors
      });
    } else {
      dispatch({
        type: 'setErrors',
        newErrors: errors.filter(erroritem => !erroritem[e.target.name])
      });
    }

    return errors[e.target.name];
  };

  const handleChange = (onChangeCustom, e) => {
    validateNotEmpty(e);
    onChangeCustom(e);

    if (touched.find(touchitem => touchitem[e.target.name]) === undefined) {
      touch[e.target.name] = true;

      touched.push(touch);

      dispatch({
        type: 'setTouched',
        newTouchedState: touched
      });
    }
  };

  const inputRef = React.createRef();

  useEffect(() => {
    if (!inputRef.current.value) {
      error[inputRef.current.name] = REQUIRED_FIELD;

      errors.push(error);

      dispatch({
        type: 'setErrors',
        newErrors: errors
      });
    }
  }, []);

  const {
    type,
    placeholder,
    classNameCustom,
    name,
    customValue,
    checked,
    disabled,
    required,
    readonly,
    maxlength,
    icon,
    onChangeCustom
  } = props;

  return (
    <div className='inpt__wrapper'>
      <input
        ref={inputRef}
        id={name}
        maxLength={maxlength}
        readOnly={readonly}
        required={required}
        disabled={disabled}
        checked={checked}
        name={name}
        value={customValue}
        type={type}
        placeholder={placeholder}
        className={`inpt ${classNameCustom} ${touched.find(
          touchitem => touchitem[name]
        ) &&
          errors.find(erroritem => erroritem[name]) &&
          required &&
          'error'}`}
        onChange={e => handleChange(onChangeCustom, e)}
      />
      {icon !== null && icon !== undefined && (
        <span className='input_icn_icon'>
          <img src={`../../resources/icn/${icon}`} alt='icon' />
        </span>
      )}
      {required &&
        touched.find(touchitem => touchitem[name]) &&
        errors.find(erroritem => erroritem[name]) && (
          <span className='message message__error'>
            <span className='capitalize'>{name}</span>
            {' field is required'}
          </span>
        )}
    </div>
  );
};

export default Input;
