import React from 'react';
import { PropTypes } from 'prop-types';

const Button = props => {
  Button.propTypes = {
    classNameCustom: PropTypes.string,
    customValue: PropTypes.string,
    disabled: PropTypes.bool,
    icon: PropTypes.string,
    onClickCustom: PropTypes.func.isRequired,
    idCustom: PropTypes.string
  };

  Button.defaultProps = {
    classNameCustom: '',
    disabled: false,
    icon: null,
    customValue: '',
    idCustom: ''
  };

  const {
    classNameCustom,
    customValue,
    disabled,
    icon,
    onClickCustom,
    idCustom
  } = props;

  return (
    <button
      id={idCustom}
      type='button'
      className={`btn ${classNameCustom}`}
      disabled={disabled}
      onClick={onClickCustom}
    >
      {icon && <i className={`uil ${icon}`} />}
      {customValue && <span>{customValue}</span>}
    </button>
  );
};

export default Button;
