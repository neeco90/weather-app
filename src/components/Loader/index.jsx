import React from 'react';

const Loader = () => (
  <div className='loader__container row middle-xs center-xs'>
    <div className='lds-ring'>
      <div />
      <div />
      <div />
      <div />
    </div>
  </div>
);

export default Loader;
