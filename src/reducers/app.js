const appReducer = (state, action) => {
  switch (action.type) {
    case 'setErrors':
      return {
        ...state,
        errors: [...action.newErrors]
      };

    case 'setTouched':
      return {
        ...state,
        touched: [...action.newTouchedState]
      };

    case 'setSearchInputs':
      return {
        ...state,
        searchData: { ...state.searchData, ...action.newSearchData }
      };

    case 'setLoader':
      return {
        ...state,
        loader: action.newLoaderState
      };

    case 'setCurrentStep':
      return {
        ...state,
        currentStep: action.newCurrentStep
      };

    case 'setResponseData':
      return {
        ...state,
        response: action.newResponse
      };

    case 'setErrorAPI':
      return {
        ...state,
        errorAPI: action.newError
      };

    case 'toggleFav':
      return {
        ...state,
        favourites: [...action.newFavourite]
      };

    default:
      return state;
  }
};

export default appReducer;
