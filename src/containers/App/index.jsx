import React from 'react';

import ErrorBoundary from '../../components/ErrorBoundary';
import { StateProvider } from '../../state';
import mainReducer from '../../reducers';
import Main from '../Main';

const initialState = {
  app: {
    loader: false,
    searchData: {
      city: '',
      country: ''
    },
    response: {},
    errors: [],
    touched: [],
    currentStep: 'weather-search',
    favourites: []
  }
};

const App = () => (
  <StateProvider initialState={initialState} reducer={mainReducer}>
    <ErrorBoundary>
      <Main />
    </ErrorBoundary>
  </StateProvider>
);

export default App;
