import React from 'react';
import uuidv1 from 'uuid/v1';

import { useStateValue } from '../../state/index';
import { LABELS } from '../../config/labels';
import Button from '../../components/Button';
import { MESSAGES } from '../../config/messages';

const FavouriteResults = () => {
  const [
    {
      app: { favourites, errorAPI, touched }
    },
    dispatch
  ] = useStateValue();

  const { FAVTITLE, LOCATION, TEMPERATURE, HUMIDITY } = LABELS;

  const { NO_FAV_MSG } = MESSAGES;

  const handleCleanForm = () => {
    dispatch({
      type: 'setSearchInputs',
      newSearchData: { city: '', country: '' }
    });

    touched.length = 0;

    dispatch({
      type: 'setTouched',
      newTouchedState: touched
    });
  };

  const handleBackButton = () => {
    dispatch({
      type: 'setErrorAPI',
      newError: null
    });

    dispatch({
      type: 'setCurrentStep',
      newCurrentStep: 'weather-search'
    });

    handleCleanForm();
  };

  const handleToggleFavourite = c => {
    const removeFav = favourites.filter(favitem => favitem.name !== c);

    dispatch({
      type: 'toggleFav',
      newFavourite: removeFav
    });
  };

  return (
    <div className='row main-content start-xs'>
      <div className='col-xs-12 vpadding-20--xs no-pd-top--xs'>
        <div className='row'>
          <div className='col-xs'>
            <div className='row start-xs middle-xs'>
              <div className='col-xs-2'>
                <Button
                  classNameCustom='btn--icon'
                  icon='uil-arrow-left'
                  onClickCustom={handleBackButton}
                />
              </div>
              <div className='col-xs-8 text-center--xs'>
                <h5 className='uppercase roboto-bold text--gray--dark--1'>
                  {FAVTITLE}
                </h5>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className='col-xs-12'>
        {errorAPI && (
          <div className='row center-xs'>
            <div className='col-xs-12'>
              <p className='roboto-bold text--error'>{errorAPI.message}</p>
            </div>
          </div>
        )}
        {!errorAPI &&
          favourites.length !== 0 &&
          favourites.map(fav => (
            <div
              className={`row vmargin-20--xs no-mg-top--xs ${favourites.length >
                1 && 'vpadding-10--xs pipe--h'}`}
              key={uuidv1()}
            >
              <div className='col-xs-12'>
                <div className='row middle-xs'>
                  <div className='col-xs-6'>
                    <h6 className='uppercase text--primary'>{LOCATION}</h6>
                  </div>
                  <div className='col-xs-6'>
                    <div className='row end-xs'>
                      <div className='col-xs-3'>
                        <Button
                          classNameCustom='btn--icon favourite'
                          icon='uil-heart'
                          onClickCustom={() => handleToggleFavourite(fav.name)}
                        />
                      </div>
                      <div className='col-xs-3'>
                        <Button
                          classNameCustom='btn--icon'
                          icon='uil-share-alt'
                          onClickCustom={handleBackButton}
                        />
                      </div>
                    </div>
                  </div>
                  <div className='col-xs-12 vpadding-10--xs'>
                    <h1 className='text--primary roboto-bold'>
                      {fav.name}
                      {','}
                      <br />
                      {fav.sys.country}
                    </h1>
                  </div>
                </div>
                <div className='row vpadding-30--xs'>
                  <div className='col-xs-4 col-sm-3'>
                    <div className='row'>
                      <div className='col-xs-12'>
                        <h6 className='uppercase text--primary'>
                          {TEMPERATURE}
                        </h6>
                      </div>
                      <div className='col-xs-12'>
                        <h2 className='text--primary'>
                          {Math.round(fav.main.temp)}
                          {'°C'}
                        </h2>
                      </div>
                    </div>
                  </div>
                  <div className='col-xs-4 col-sm-3'>
                    <div className='row'>
                      <div className='col-xs-12'>
                        <h6 className='uppercase text--primary'>{HUMIDITY}</h6>
                      </div>
                      <div className='col-xs-12'>
                        <h2 className='text--primary'>
                          {fav.main.humidity}
                          {'%'}
                        </h2>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          ))}
        {!errorAPI && favourites.length === 0 && (
          <div className='row center-xs'>
            <div className='col-xs-12'>
              <p className='roboto-bold'>{NO_FAV_MSG}</p>
            </div>
          </div>
        )}
      </div>
    </div>
  );
};

export default FavouriteResults;
