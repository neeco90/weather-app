import React from 'react';

import Home from '../Home';
import Loader from '../../components/Loader';
import { useStateValue } from '../../state';

const Main = () => {
  const [
    {
      app: { loader }
    }
  ] = useStateValue();

  return <div className='col-xs-12'>{loader ? <Loader /> : <Home />}</div>;
};

export default Main;
