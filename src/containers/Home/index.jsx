import React from 'react';

import WeatherSearch from '../WeatherSearch';
import WeatherResult from '../WeatherResult';
import FavouriteResults from '../FavouriteResults';
import { useStateValue } from '../../state/index';

const Home = () => {
  const [
    {
      app: { currentStep }
    }
  ] = useStateValue();

  return (
    <div className='row center-xs'>
      <div className='col-xs col-lg-4'>
        {currentStep === 'weather-search' && <WeatherSearch />}
        {currentStep === 'weather-result' && <WeatherResult />}
        {currentStep === 'favourite-results' && <FavouriteResults />}
      </div>
    </div>
  );
};

export default Home;
