import React from 'react';

import { useStateValue } from '../../state/index';
import { LABELS } from '../../config/labels';
import Button from '../../components/Button';

const WeatherResult = () => {
  const [
    {
      app: { response, touched, favourites, errorAPI }
    },
    dispatch
  ] = useStateValue();

  const { LOCATION, TEMPERATURE, HUMIDITY } = LABELS;

  const handleCleanForm = () => {
    dispatch({
      type: 'setSearchInputs',
      newSearchData: { city: '', country: '' }
    });

    touched.length = 0;

    dispatch({
      type: 'setTouched',
      newTouchedState: touched
    });
  };

  const handleBackButton = () => {
    handleCleanForm();

    dispatch({
      type: 'setErrorAPI',
      newError: null
    });

    dispatch({
      type: 'setCurrentStep',
      newCurrentStep: 'weather-search'
    });
  };

  const findCity = favourites.find(favitem => favitem.name === response.name);

  const handleToggleFavourite = c => {
    if (findCity === undefined) {
      favourites.push(response);

      dispatch({
        type: 'toggleFav',
        newFavourite: favourites
      });
    } else {
      const removeFav = favourites.filter(favitem => favitem.name !== c);

      dispatch({
        type: 'toggleFav',
        newFavourite: removeFav
      });
    }
  };

  return (
    <div className='row main-content middle-xs start-xs'>
      <div className='col-xs-12 self-start'>
        <div className='row'>
          <div className='col-xs'>
            <div className='row start-xs'>
              <div className='col-xs-12'>
                <Button
                  classNameCustom='btn--icon'
                  icon='uil-arrow-left'
                  onClickCustom={handleBackButton}
                />
              </div>
            </div>
          </div>
          {!errorAPI && (
            <div className='col-xs'>
              <div className='row end-xs'>
                <div className='col-xs-3'>
                  <Button
                    classNameCustom={`btn--icon ${findCity && 'favourite'}`}
                    icon='uil-heart'
                    onClickCustom={() => handleToggleFavourite(response.name)}
                  />
                </div>
                <div className='col-xs-3'>
                  <Button
                    classNameCustom='btn--icon'
                    icon='uil-share-alt'
                    onClickCustom={handleBackButton}
                  />
                </div>
              </div>
            </div>
          )}
        </div>
      </div>
      <div className='col-xs-12 self-start'>
        {errorAPI ? (
          <div className='row center-xs'>
            <div className='col-xs-12'>
              <p className='roboto-bold text--error'>{errorAPI.message}</p>
            </div>
          </div>
        ) : (
          <div className='row'>
            <div className='col-xs-12'>
              <div className='row'>
                <div className='col-xs-12'>
                  <h6 className='uppercase text--primary'>{LOCATION}</h6>
                </div>
                <div className='col-xs-12 vpadding-10--xs'>
                  <h1 className='text--primary roboto-bold'>
                    {response.name}
                    {','}
                    <br />
                    {response.sys.country}
                  </h1>
                </div>
              </div>
              <div className='row vpadding-30--xs'>
                <div className='col-xs-4 col-sm-3'>
                  <div className='row'>
                    <div className='col-xs-12'>
                      <h6 className='uppercase text--primary'>{TEMPERATURE}</h6>
                    </div>
                    <div className='col-xs-12'>
                      <h2 className='text--primary'>
                        {Math.round(response.main.temp)}
                        {'°C'}
                      </h2>
                    </div>
                  </div>
                </div>
                <div className='col-xs-4 col-sm-3'>
                  <div className='row'>
                    <div className='col-xs-12'>
                      <h6 className='uppercase text--primary'>{HUMIDITY}</h6>
                    </div>
                    <div className='col-xs-12'>
                      <h2 className='text--primary'>
                        {response.main.humidity}
                        {'%'}
                      </h2>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        )}
      </div>
    </div>
  );
};

export default WeatherResult;
