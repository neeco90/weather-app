import React from 'react';

import { MESSAGES } from '../../config/messages';

import { Main } from '..';

const { NOT_FOUND_MSG } = MESSAGES;

const NotFound = () => {
  const renderContent = () => <div>{NOT_FOUND_MSG}</div>;

  const props = {
    withHeader: false,
    content: renderContent
  };

  return <Main {...props} />;
};

export default NotFound;
