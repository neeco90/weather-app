import React from 'react';
import axios from 'axios';

import { useStateValue } from '../../state/index';
import Input from '../../components/Input';
import Button from '../../components/Button';
import { LABELS } from '../../config/labels';

const WeatherSearch = () => {
  const [
    {
      app: {
        searchData: { city, country },
        errors,
        favourites
      }
    },
    dispatch
  ] = useStateValue();

  const handleChangeInput = e => {
    dispatch({
      type: 'setSearchInputs',
      newSearchData: { [e.target.name]: e.target.value }
    });
  };

  const handleNextStep = () => {
    dispatch({
      type: 'setCurrentStep',
      newCurrentStep: 'weather-result'
    });
  };

  const handleSearch = async () => {
    dispatch({
      type: 'setLoader',
      newLoaderState: true
    });

    try {
      const result = await axios.get(
        `https://api.openweathermap.org/data/2.5/weather?q=${city},${country}&units=metric&APPID=12573b21e7f057f3aea30a4ec4943466`
      );

      dispatch({
        type: 'setLoader',
        newLoaderState: false
      });

      dispatch({
        type: 'setResponseData',
        newResponse: result.data
      });
    } catch (error) {
      dispatch({
        type: 'setLoader',
        newLoaderState: false
      });

      dispatch({
        type: 'setErrorAPI',
        newError: error
      });
    }

    handleNextStep();
  };

  const handleFavSection = async () => {
    if (favourites.length !== 0) {
      dispatch({
        type: 'setLoader',
        newLoaderState: true
      });

      try {
        const cityIDs = favourites.map(fav => fav.id).join(',');

        const result = await axios.get(
          `https://api.openweathermap.org/data/2.5/group?id=${cityIDs}&units=metric&APPID=12573b21e7f057f3aea30a4ec4943466`
        );

        dispatch({
          type: 'setResponseData',
          newResponse: result.data
        });

        dispatch({
          type: 'setCurrentStep',
          newCurrentStep: 'favourite-results'
        });

        dispatch({
          type: 'setLoader',
          newLoaderState: false
        });
      } catch (error) {
        dispatch({
          type: 'setLoader',
          newLoaderState: false
        });

        dispatch({
          type: 'setErrorAPI',
          newError: error
        });
      }
    } else {
      dispatch({
        type: 'setCurrentStep',
        newCurrentStep: 'favourite-results'
      });
    }
  };

  const { MAINTITLE } = LABELS;

  return (
    <div className='row middle-xs main-content center-xs'>
      <div className='col-xs-12'>
        <div className='row vmargin-40--xs no-mg-top--xs'>
          <div className='col-xs-12'>
            <h1 className='text--primary roboto-bold'>{MAINTITLE}</h1>
          </div>
        </div>
        <div className='row start-xs'>
          <div className='col-xs-12 vpadding-20--xs'>
            <Input
              type='text'
              name='city'
              customValue={city}
              onChangeCustom={handleChangeInput}
              classNameCustom='full-width'
              placeholder='City'
              required
            />
          </div>
          <div className='col-xs-12'>
            <Input
              type='text'
              name='country'
              customValue={country}
              onChangeCustom={handleChangeInput}
              classNameCustom='full-width'
              placeholder='Country'
              required
            />
          </div>
        </div>
        <div className='row'>
          <div className='col-xs-12 vpadding-20--xs'>
            <Button
              classNameCustom='btn--primary'
              customValue='Search'
              onClickCustom={handleSearch}
              disabled={errors.length !== 0}
            />
          </div>
        </div>
        <div className='row'>
          <div className='col-xs-12'>
            <Button
              classNameCustom='btn--link'
              customValue='Go to my favourites'
              onClickCustom={handleFavSection}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default WeatherSearch;
