const MESSAGES = {
  BOUNDARY_ERROR_MSG: 'Oops! something went wrong',
  NO_FAV_MSG: `You don't have favourites added yet`,
  REQUIRED_FIELD: 'Este campo es requerido'
};

export { MESSAGES };
