const LABELS = {
  MAINTITLE: 'Weather Finder',
  LOCATION: 'Location',
  TEMPERATURE: 'Temperature',
  HUMIDITY: 'Humidity',
  FAVTITLE: 'My Favourites',

  VOID_FUNC: () => {}
};

export { LABELS };
